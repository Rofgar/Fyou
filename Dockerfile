FROM rust:slim-bullseye

WORKDIR /usr/src/fyou
COPY . .
RUN cargo install --path .
EXPOSE 5676/udp
ENTRYPOINT ["f-you"]