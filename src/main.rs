use std::net::{IpAddr, Ipv4Addr, SocketAddr, UdpSocket};
use std::str;
use std::thread::sleep;
use std::time::Duration;

use interfaces::{Interface, Kind};
use rand::seq::SliceRandom;
use regex::Regex;

fn get_network_interface_addresses() -> Vec<IpAddr> {
    return Interface::get_all().expect("could not get interfaces")
        .iter()
        .map(
            |i| i.addresses
                .clone()
                .into_iter()
                .filter(|a| a.kind == Kind::Ipv4)
                .map(|a| a.addr)
                .filter(|o| o.is_some() && o.unwrap().ip() != Ipv4Addr::LOCALHOST)
                .map(|o| o.unwrap().ip())
                .collect::<Vec<_>>()
        )
        .flatten()
        .collect();
}

fn main() {
    let names = vec!["Tom", "Jerry", "Viky", "Bob"];
    let my_name = names.choose(&mut rand::thread_rng()).unwrap();

    println!("{} is born", my_name);

    let personal_greeting_regex = Regex::new(r"(Hello )(.+)(, I am )(.+)").unwrap();
    let general_greeting_regex = Regex::new(r"(Hi everyone, I am )(.+)").unwrap();
    let warm_greeting_regex = Regex::new(r"(Flark you )(.+)").unwrap();

    let broadcast_interface = "0.0.0.0:5676";
    let broadcast_socket: SocketAddr = broadcast_interface.parse().expect(&*format!("Could not parse interface {}", broadcast_interface));

    let socket = UdpSocket::bind(broadcast_socket).expect("Couldn't bind to address");
    socket.set_broadcast(true).expect("Could not set socket to broadcast mode");

    let message = format!("Hi everyone, I am {}", my_name);
    let message_bytes = message.as_bytes();


    let ipv4_regex = Regex::new(r"(.+\..+\..+)\.(.+)").unwrap();

    for ip in get_network_interface_addresses() {
        let ip_string = ip.to_string();

        let captures = ipv4_regex.captures(&ip_string).unwrap();
        let ip_base = captures.get(1).unwrap().as_str();
        let ip_broadcast = format!("{}.0", ip_base);

        socket.send_to(message_bytes, format!("{}:5676", ip_broadcast))
            .map(|_| println!("Broadcasted on interface: {}", ip_broadcast))
            .map_err(|e| {
                println!("Failed to broadcast on interface: {}", e);
                ();
            }).unwrap_or(());
    }

    loop {
        let mut buffer = [0; 1024];
        let (number_of_bytes, source_address) =
            socket.recv_from(&mut buffer).expect("Didn't receive data");

        if get_network_interface_addresses().contains(&source_address.ip()) {
            println!("Skipping talking to myself");
            continue;
        }

        let filled_buffer = &mut buffer[..number_of_bytes];
        let message = str::from_utf8(&filled_buffer).unwrap();
        println!("Got message [{}]: {}", source_address, message);

        if message == "Flark you too!" {
            // Ignore rudeness
            continue
        }

        match general_greeting_regex.captures(message) {
            Some(captures) => {
                socket.send_to(
                    format!("Hello {}, I am {}", captures.get(2).map_or("Kinder", |m| m.as_str()), my_name).as_bytes(),
                    source_address,
                ).expect("Could not send my general greeting");
                ()
            }
            None => ()
        }

        match personal_greeting_regex.captures(message) {
            Some(captures) => {
                socket.send_to(
                    format!("Flark you {}!", captures.get(4).map_or("Zum", |m| m.as_str())).as_bytes(),
                    source_address,
                ).expect("Could not send my personal greeting");
                ()
            }
            None => ()
        }

        match warm_greeting_regex.captures(message) {
            Some(_) => {
                socket.send_to("Flark you too!".as_bytes(), source_address).expect("Could not send my warm greeting");
                ()
            }
            None => ()
        }
        sleep(Duration::from_millis(100))
    }
}
